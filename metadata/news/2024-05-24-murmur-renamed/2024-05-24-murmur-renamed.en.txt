Title: voip/murmur has been renamed to voip/mumble-server
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2024-05-24
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: voip/murmur

Please install voip/mumble-server and *afterwards* uninstall voip/murmur.

1. Take note of any packages depending on voip/murmur:
cave resolve \!voip/murmur

2. Install voip/mumble-server:
cave resolve voip/mumble-server -x

3. Re-install the packages from step 1.

4. Uninstall voip/murmur:
cave resolve \!voip/murmur -x

Do it in *this* order or you'll potentially *break* your system.
