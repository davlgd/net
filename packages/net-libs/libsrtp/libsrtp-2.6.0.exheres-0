# Copyright 2013, 2015 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require github [ user=cisco tag=v${PV} ] meson

SUMMARY="Secure Real-time Transport Protocol (SRTP) library"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES+="
    build:
        virtual/pkg-config
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    # Other available choices are nss mbedtls
    -Dcrypto-library=openssl
    -Dcrypto-library-kdf=disabled
    -Ddebug-logging=false
    -Ddoc=disabled
    -Dfuzzer=disabled
    -Dlog-file=''
    -Dlog-stdout=false
    -Dpcap-tests=disabled
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

src_prepare() {
    meson_src_prepare

    # Don't bind to 0.0.0.0
    edo sed -e "s|0\.0\.0\.0|127.0.0.1|" -i test/rtpw_test.sh
}

src_test() {
    esandbox allow_net --bind "inet:LOCAL@9999"
    meson_src_test
    esandbox disallow_net --bind "inet:LOCAL@9999"
}

