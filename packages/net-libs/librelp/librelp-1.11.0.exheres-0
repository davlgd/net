# Copyright 2009-2010 Nathan McSween
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A library for the relp protocol"
DESCRIPTION="
librelp is an easy to use library for the RELP protocol. RELP in turn provides
reliable event logging over the network (and consequently RELP stands for Reliable
Event Logging Protocol).
"
HOMEPAGE="https://www.rsyslog.com/${PN}"
DOWNLOADS="http://download.rsyslog.com/${PN}/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

# Tests are fishy, some fail with openssl others with gnutls
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        providers:gnutls? ( dev-libs/gnutls[>=2.0.0] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-debug
    --disable-static
    --disable-valgrind
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    "providers:gnutls tls"
)

src_configure() {
    local myconf=(
        "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do \
            option_enable ${s} ; \
        done )
    )

    if option providers:libressl || option providers:openssl; then
        myconf+=( --enable-tls-openssl )
    else
        myconf+=( --disable-tls-openssl )
    fi

    econf "${myconf[@]}"
}

src_test() {
    esandbox allow_net "inet:0.0.0.0@31514"
    esandbox allow_net --connect "inet:127.0.0.1@31514"
    emake -j1 check
    esandbox disallow_net --connect "inet:127.0.0.1@31514"
    esandbox disallow_net "inet:0.0.0.0@31514"
}

