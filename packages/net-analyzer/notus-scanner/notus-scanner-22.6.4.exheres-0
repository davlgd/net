# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi [ pnv=${PNV/-/_} ] \
    py-pep517 [ backend=poetry-core test=unittest entrypoints=[ ${PN}-{scan-start,scanner,subscriber} ] work=${PNV/-/_} blacklist=3.8 ] \
    systemd-service

SUMMARY="A vulnerability scanner for creating results from local security checks (LSCs)"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        group/gvm
        user/gvm
        dev-python/packaging[>=24.1][python_abis:*(-)?]
        dev-python/paho-mqtt[>=1.6&<3.0][python_abis:*(-)?]
        dev-python/psutil[>=6.0&<7.0][python_abis:*(-)?]
        dev-python/python-gnupg[>=0.5.1&<0.6.0][python_abis:*(-)?]
        python_abis:3.9? ( dev-python/tomli[<3.0.0][python_abis:3.9] )
        python_abis:3.10? ( dev-python/tomli[<3.0.0][python_abis:3.10] )
    run:
        net/mosquitto
"

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    # remove version constraint
    edo sed \
        -e 's:packaging = "<24.2":packaging = ">=24.1":g' \
        -i pyproject.toml
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    install_systemd_files

    insinto /etc/gvm
    doins "${FILES}"/notus-scanner.toml

    keepdir /var/log/gvm
    edo chown gvm:gvm "${IMAGE}"/var/log/gvm
}

test_one_multibuild() {
    # FAIL: test_defaults (tests.cli.test_cli_parser.CliParserTestCase)
    # AssertionError: '/var/log/gvm/notus-scanner.log' is not None
    edo rm tests/cli/test_cli_parser.py

    py-pep517_run_tests_unittest
}

