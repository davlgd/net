# Copyright 2020-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Requarks project=wiki release=v${PV} pnv=${PN} suffix=tar.gz ] \
    systemd-service

SUMMARY="Modern, lightweight and powerful wiki app built on NodeJS"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        user/${PN}
        group/${PN}
    run:
        dev-lang/node[>=12.0]
    recommendation:
        dev-db/postgresql:*[>=9.5] [[
            description = [ Highly recommended database backend ]
        ]]
    suggestion:
        dev-scm/git[>=2.7.4] [[
            description = [ Optional Git storage backend to store documentation ]
        ]]
"

WORK=${WORKBASE}

src_prepare() {
    # set sane default dataPath
    edo sed \
        -e 's#dataPath: ./data#dataPath: /var/lib/wiki-js/data#' \
        -i config.sample.yml

    default
}

src_compile() {
    :
}

src_test() {
    :
}

src_install() {
    insinto /etc/${PN}
    newins config.sample.yml config.yml
    edo chown -R ${PN}:${PN} "${IMAGE}"/etc/${PN}
    edo chmod 0750 "${IMAGE}"/etc/${PN}

    insinto /var/lib/${PN}
    doins -r "${WORK}"/*

    keepdir /var/lib/${PN}/data
    edo chown ${PN}:${PN} "${IMAGE}"/var/lib/${PN}/data

    install_systemd_files
}

