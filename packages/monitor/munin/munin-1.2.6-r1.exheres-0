# Copyright (c) 2009 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Munin the monitoring tool surveys all your computers and remembers what it saw."
DESCRIPTION="
Munin presents all the information in graphs through a web interface. Its emphasis is on plug and play
capabilities. After completing a installation a high number of monitoring plugins will be playing
with no more effort.

Using Munin you can easily monitor the performance of your computers, networks, SANs, applications,
weather measurements and whatever comes to mind. It makes it easy to determine 'what's different
today' when a performance problem crops up. It makes it easy to see how you're doing capacity-wise
on any resources.

Munin uses the excellent RRDTool (written by Tobi Oetiker) and the framework is written in Perl,
while plugins may be written in any language. Munin has a master/node architecture in which the
master connects to all the nodes at regular intervals and asks them for data. It then stores the
data in RRD files, and (if needed) updates the graphs. One of the main goals has been ease of
creating new plugins (graphs).
"
HOMEPAGE="http://munin-monitoring.org/"
DOWNLOADS="mirror://sourceforge/munin/${PV}/${PN}_${PV}.tar.gz"

LICENCES="GPL-2 bitstream-font"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc                         [[ description = [ Build and install munin documentation ] ]]
    (
        server                  [[ description = [ The Munin server which polls the clients and creates graphs ] ]]
        client                  [[ description = [ Plugin data made available to a Munin server through a TCP server ] ]]
    ) [[ number-selected = at-least-one ]]"

DEPENDENCIES="
    build:
        doc? (
            app-text/html2text
            app-text/htmldoc
        )
    build+run:
        client? (
            user/munin
        )
        group/munin
    run:
        dev-perl/Net-Server
        server? (
            net-analyzer/rrdtool[perl]
            dev-perl/HTML-Template
        )
    suggestion:
        net-firewall/iptables   [[ description = [ Needed for ip_ plugin to avoid wrap-around (if_ plugin) when monitoring network traffic ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/remove-chown-chmod-from-Makefile.patch )
DEFAULT_SRC_COMPILE_PARAMS=( default )

src_configure(){
    edo cp "${FILES}"/Makefile.config "${WORK}"/
}

src_install(){
    local mymake=()

    if option server; then
        mymake+=( install-main )
    fi
    if option client; then
        mymake+=( install-node install-node-plugins )
    fi
    if option doc; then
        mymake+=( install-doc install-man )
    fi
    emake DESTDIR="${IMAGE}" "${mymake[@]}"

    #/etc/munin: plugins/ symlinks to plugins in use
    #            plugin-conf.d/ plugin configuration
    if option client; then
        keepdir \
            /etc/munin/plugins \
            /etc/munin/plugin-conf.d
        #FIXME test whether this is actually needed
        edo rmdir "${IMAGE}"/usr/${LIBDIR}/perl5/site_perl/*/Munin/Plugin
    fi

    # Fix LIBDIR crap.
    if [[ ${LIBDIR} != lib ]] ; then
        if [[ -d ${IMAGE}/usr/lib/munin ]] ; then
            edo mv "${IMAGE}"/usr/{lib,${LIBDIR}}/munin
        fi
        [[ -d ${IMAGE}/usr/lib ]] && edo rmdir "${IMAGE}"/usr/lib
    fi

    keepdir /var/cache/munin
    keepdir /var/cache/munin/plugin-state
    edo chown munin:munin "${IMAGE}"/var/cache/munin

    keepdir /var/log/munin
    edo chown :munin "${IMAGE}"/var/log/munin
    edo chmod 0775 "${IMAGE}"/var/log/munin

    edo rmdir "${IMAGE}"/run/{munin,}

    emagicdocs
}

