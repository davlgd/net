# Copyright 2017-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="${PV/_}"
MY_PNV="check-mk-raw-${MY_PV}.cre"

require systemd-service [ systemd_files=[ agents/scripts/super-server/0_systemd/{check-mk-agent{-async.service,.socket,@.service},cmk-agent-ctl-daemon.service} ] ]

SUMMARY="Checkmk Agent for Linux"
DESCRIPTION="
The Checkmk Agent uses xinetd or systemd to provide information about the system on TCP port 6556.
This can be used to monitor the host via Checkmk.
"
HOMEPAGE="https://checkmk.com"
DOWNLOADS="https://download.checkmk.com/checkmk/${MY_PV}/${MY_PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="docker mysql postgresql xinetd"

RESTRICT="strip"

DEPENDENCIES="
    build+run:
        group/cmk-agent
        user/cmk-agent
    run:
        docker? ( dev-python/docker-py[>=2.0.0] )
        xinetd? ( sys-apps/xinetd )
    recommendation:
        dev-lang/python:*[>=3] [[
            description = [ Required by some plugins written in Python ]
        ]]
"

WORK=${WORKBASE}/${MY_PNV}

src_configure() {
    :
}

src_compile() {
    :
}

src_test() {
    :
}

src_install() {
    newbin agents/check_mk_agent.linux check_mk_agent
    newbin agents/check_mk_caching_agent.linux check_mk_caching_agent
    dobin agents/linux/cmk-agent-ctl
    dobin agents/mk-job

    keepdir /etc/check_mk
    keepdir /var/lib/check_mk_agent
    keepdir /usr/$(exhost --target)/lib/check_mk_agent/{,plugins,local,job,spool}

    keepdir /var/lib/cmk-agent
    edo chown cmk-agent:cmk-agent "${IMAGE}"/var/lib/cmk-agent

    install_systemd_files

    if option xinetd ; then
        insinto /etc/xinetd.d
        doins agents/scripts/super-server/1_xinetd/check-mk-agent
    fi

    # Think about making the installation of the plugins configurable via SUBOPTIONS
    exeinto /usr/$(exhost --target)/lib/check_mk_agent/plugins
    doexe agents/plugins/*
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/*.{aix,freebsd,solaris}

    insinto /etc/check_mk
    doins agents/cfg_examples/*.cfg

    # do not install by default
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/{dnsclient,Makefile,mk_tinkerforge.py,plesk_*,README}
    edo rm "${IMAGE}"/etc/check_mk/{docker.minimal,docker.perf_tradeoff,encryption,filestats,real_time_checks}*.cfg
    # mk_mongodb requires pymongo
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/mk_mongodb.py
    # mk_jolokia requires requests and simplejson
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/mk_jolokia.py
    edo rm "${IMAGE}"/etc/check_mk/jolokia.cfg
    # mk_site_object_counts is only useful on the checkmk server itself
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/mk_site_object_counts
    edo rm "${IMAGE}"/etc/check_mk/site_object_counts.cfg

    if ! option docker; then
        edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/mk_docker.py
        edo rm "${IMAGE}"/etc/check_mk/docker.cfg
    fi

    if ! option mysql; then
        edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/mk_mysql
        edo rm "${IMAGE}"/etc/check_mk/mysql.cfg
    fi

    ! option postgresql && edo rm "${IMAGE}"/usr/$(exhost --target)/lib/check_mk_agent/plugins/mk_postgres.py
}

